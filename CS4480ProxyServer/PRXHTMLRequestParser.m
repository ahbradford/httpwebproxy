//
//  PRXHTMLRequestParser.m
//  CS4480ProxyServer
//
//  Created by Adam Bradford on 1/12/14.
//  Copyright (c) 2014 Adam Bradford. All rights reserved.
//

#import "PRXHTMLRequestParser.h"

@implementation PRXHTMLRequestParser
-(instancetype)init
{
    self = [super init];
    if(self)
    {
        
        
    }
    return self;
}

//this just splits the string into a dictionary to look up html options
-(void)setRequestString:(NSString *)requestString
{
    requestString = [requestString stringByReplacingOccurrencesOfString:@"\r\n\r\n" withString:@""];
    _requestString = requestString;
    _dict = [[NSMutableDictionary alloc]init];
    
    NSArray * array = [requestString componentsSeparatedByString:@"\r\n"];
    
    [array enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSString *stringUTT = array[idx];
        
        if(idx == 0)
        {
		   NSRange firstSpace = [stringUTT rangeOfString:@" "];
		   if(firstSpace.location != NSNotFound)
		   {
			   _dict[@"Method"] = [stringUTT substringToIndex:firstSpace.location];
			   
			   stringUTT = [stringUTT substringFromIndex:firstSpace.location + 1];
			   
			   NSRange urlEndRange = [stringUTT rangeOfString:@" "];
			   if(urlEndRange.location != NSNotFound)
			   {
				   _dict[@"URL"]  = [stringUTT substringToIndex:urlEndRange.location];
				   _dict[@"Version"] = [stringUTT substringFromIndex:urlEndRange.location + 1];
			   }
		   }
		   
		   
        }
        
        else
        {
            
            if([stringUTT isKindOfClass:[NSString class]])
            {
                NSArray *components = [array[idx] componentsSeparatedByString:@":"];
                if(components.count != 2)
                {
                    
                }
                
                if(components.count < 2)
                {
                    *stop = YES;
                    return;
                }
                NSString *key = components[0];
			   NSString *value = [components[1] substringFromIndex:1];
			 
			  if([key isEqualToString:@"Host"])
			  {
				 if(components.count == 3)
				 {
					 self.dict[@"Port"] = components[2];
				 }
			  }

                self.dict[key] = value;
            }
            else
            {
                *stop = YES;
            }
        }
    }];
}

@end
