//
//  PRXHTMLRequestParser.h
//  CS4480ProxyServer
//
//  Created by Adam Bradford on 1/12/14.
//  Copyright (c) 2014 Adam Bradford. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PRXHTMLRequestParser : NSObject
@property (strong,nonatomic) NSString *requestString;
@property (strong,readonly) NSMutableDictionary *dict;

@end
