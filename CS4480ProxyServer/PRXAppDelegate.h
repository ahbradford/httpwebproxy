//
//  PRXAppDelegate.h
//  CS4480ProxyServer
//
//  Created by Adam Bradford on 1/12/14.
//  Copyright (c) 2014 Adam Bradford. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface PRXAppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
