//
//  PRXProxyServer.m
//  CS4480ProxyServer
//
//  Created by Adam Bradford on 1/12/14.
//  Copyright (c) 2014 Adam Bradford. All rights reserved.
//

#import "PRXProxyServer.h"
#import "PRXHTMLRequestParser.h"
#import <sys/socket.h>
#import <netinet/in.h>
#import <arpa/inet.h>
#import <ifaddrs.h>
#import <netdb.h>
#import "PRXCacheManager.h"

#define zeroize(ptr, size) memset (ptr, 0, size)
#define MAXSIZE 2048
#define TIMEOUT 10
#define HTTP_VERSION_NOT_SUPPORTED 505
#define PAGENOTFOUND 404
#define NOTIMPLEMENTED 501
#define BAD_REQUEST 400

@interface PRXProxyServer()
@property (nonatomic) int socketID;
@property (nonatomic) BOOL delegateRequestedStop;
@property (nonatomic,strong) NSOperationQueue *listenQueue;
@property (nonatomic,strong) NSOperationQueue *hostFetchQueue;


@property (nonatomic) BOOL requestedProxyStop;
@property (nonatomic,strong) PRXCacheManager *cache;

@end

@implementation PRXProxyServer

-(instancetype)init
{
	self = [super init];
	if(self)
	{
		//initalize the cache
		self.cache = [[PRXCacheManager alloc]init];
	}
	return self;
}


-(int)currentConnections
{
    return (int)self.hostFetchQueue.operations.count;
}

	//start the proxy app
-(BOOL)startProxyWithPort:(int)port
{
	//is the port even valid?
	if(port <= 0)
	{
		[_delegate messageFromProxy:@"Port Must be greater than 0"];
		return NO;
	}
	
	//clear the cache
	[self.cache clearCache];
	
	//get the binded socket
	self.socketID = [self beginListeningOnPort:port];
	
	//set up the operations queues
	self.hostFetchQueue = [[NSOperationQueue alloc]init];
	[self.hostFetchQueue setMaxConcurrentOperationCount:NSOperationQueueDefaultMaxConcurrentOperationCount];
	self.listenQueue = [[NSOperationQueue alloc]init];
	
	//start listening
	[self.listenQueue addOperationWithBlock:^{
		[self listenForConnections];
	}];
	
	
	self.requestedProxyStop = NO;
	
	//update the delegate
	[_delegate messageFromProxy:[NSString stringWithFormat: @"Proxy Server Started On Port: %d IP %@:",port,[self getIPAddress]]];
	return YES;
}

-(void)emptyCache
{
	[self.cache clearCache];
}

-(void)stopProxy
{
	//clear out everything and close all connections
	self.requestedProxyStop = YES;
	close(self.socketID);
	[self.listenQueue cancelAllOperations];
	[self.hostFetchQueue cancelAllOperations];
	self.listenQueue = nil;
	self.hostFetchQueue = nil;
	[self.cache clearCache];
	[_delegate messageFromProxy:@"Proxy Server Stopped"];
}

-(void)listenForConnections
{
	while (true)
		
	{
		//did the user request stop?
		if(self.requestedProxyStop)
		{
			close(self.socketID);
			break;
		}
		
		//set up and wait for a client to connect
		struct sockaddr_in clientSocket;
		socklen_t len = sizeof(clientSocket);
		
		//block until we get an incoming connection
		int accepted = accept(self.socketID, (struct sockaddr *) &clientSocket, &len);
		
		//dispatch async
		[self.hostFetchQueue addOperationWithBlock:^{
            
            
			
			
			NSString *bufferString = @"";
			
			//block and wait for a read
			
			//read the data from teh socket
			NSData *data = [self readRequestFromSocket:accepted];
			
			if(data)
			{
                
                
				
				//append whatever we read to the buffer string
				bufferString = [[NSString alloc]initWithData:data encoding:NSASCIIStringEncoding];
				
				//replace http 1.1 with http 1.0 so the remote socket will close automatically
				//bufferString = [bufferString stringByReplacingOccurrencesOfString:@"HTTP/1.1" withString:@"HTTP/1.0"];
				
				//tell the remote to clost the connection
				bufferString = [bufferString stringByReplacingOccurrencesOfString:@"Connection: keep-alive" withString:@"Connection: close"];
				
				//create a parser and parse the string
				PRXHTMLRequestParser *parser = [[PRXHTMLRequestParser alloc]init];
				parser.requestString = bufferString;
                
                //check for errors
                
                
                NSString *version = parser.dict[@"Version"];
                
                if([version rangeOfString:@"HTTP"].location == NSNotFound)
                {
                    NSData *errorData = [self stringForError:BAD_REQUEST];
                    [self writeData:errorData toSocket:accepted];
                    close(accepted);
                    NSLog(@"Bad Request");
                    return;
                }
                
                
                if(!(parser.dict[@"Method"] && parser.dict[@"Version"] && parser.dict[@"URL"] ))
                {
                    NSData *errorData = [self stringForError:BAD_REQUEST];
                    [self writeData:errorData toSocket:accepted];
                    close(accepted);
                    NSLog(@"Bad Request");
                    return;
                }
                if(![[parser.dict[@"Version"] uppercaseString] isEqualToString:@"HTTP/1.0"] )
                {
                    NSData *errorData = [self stringForError:HTTP_VERSION_NOT_SUPPORTED];
                    [self writeData:errorData toSocket:accepted];
                    close(accepted);
                    NSLog(@"Incorrect HTTP Version %@",parser.dict[@"Version"]);
                    return;
                }
                if(![[parser.dict[@"Method"] uppercaseString]  isEqualToString:@"GET" ])
                {
                    NSData *errorData = [self stringForError:NOTIMPLEMENTED];
                    [self writeData:errorData toSocket:accepted];
                    close(accepted);
                    NSLog(@"Post Not Supported");
                    return;
                }
                
                
				
				
				
				//create a filename to check the cache
				NSString *requestString = [NSString stringWithFormat:@"%@.%@.%@.cache",parser.dict[@"Method"], parser.dict[@"Host"],parser.dict[@"URL"]];
				requestString = [requestString stringByReplacingOccurrencesOfString:@"http://" withString:@""];
                
                
                
                //ok, we have a request that did not specify the host..., lets build it special, this will be handled more eleglangtly in the future....
                if(!parser.dict[@"Host"])
                {
                    NSString *host = [self hostFromHTTPURL:bufferString];
                    if(!host)
                    {
                        //error
                    }
                    else
                    {
                        parser.dict[@"Host"] = host;
                    }
                }
				
				//ask the cache for data
				NSData *receiveData = [self.cache cachedDataForRequest:requestString];
				
				//if we did get data
				if(receiveData)
				{
					//write it back to the client and update the ui.
					[self writeData:receiveData toSocket:accepted];
					[_delegate messageFromProxy:[parser.dict[@"Host"] stringByAppendingString:@"-|CACHED|"]];
				}
				
				else
				{
					//we didn't have a cache, so open a new connection to the remote server
					int remoteSocket = [self createConnectionToRemoteHost:parser.dict[@"Host"] onPort:parser.dict[@"Port"]];
                    
                    if(remoteSocket < 0)
                    {
                        NSData *errorData = [self stringForError:PAGENOTFOUND];
                        [self writeData:errorData toSocket:accepted];
                        close(accepted);
                        return;
                    }
					
					//update UI
					[_delegate messageFromProxy:parser.dict[@"Host"]];
					if(remoteSocket > 0)
					{
						//turn the modified request into bytes
						NSData *request = [bufferString dataUsingEncoding:NSUTF8StringEncoding];
						if(request)
						{
							//write data to the socket
							[self writeData:request toSocket:remoteSocket];
							
							//get some data back
							NSData *receiveData = [self readDataFromSocket:remoteSocket];
							
							//if we got data, cache it and send it to the client
							if(receiveData)
							{
								[self.cache cacheData:receiveData forRequest:requestString];
								[self writeData:receiveData toSocket:accepted];
							}
						}
						
					}
					else
					{
						//the remote
						[self writeData:[self stringForError:HTTP_VERSION_NOT_SUPPORTED] toSocket:accepted];
					}
					
					
				}
				//closet the socket
				close(accepted);
			}
			
		}];
		
        
       
		
		NSLog(@"Connected");
	}
}

-(int)createConnectionToRemoteHost:(NSString *)host onPort:(NSString *)port
{
	//make an address info struct
	int status;
	struct addrinfo hints;
	struct addrinfo *servinfo;
	
    //zero the struct and set optaions
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM; // TCP stream sockets
	
    //set the port to default if none provided
	if(!port)
		port = @"80";
	
	// get ready to connect
	status = getaddrinfo([host cStringUsingEncoding:NSASCIIStringEncoding], [port cStringUsingEncoding:NSUTF8StringEncoding], &hints, &servinfo);
	
	if(status != 0)
	{
		NSLog(@"Error getting host info for %@",host);
		freeaddrinfo(servinfo);
		return -1;
	}
	
	
	
	int sockfd = 0;
	
    //attempt to make a socket
	if((sockfd = socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol))< 0)
	{
		printf("\n Error : Could not create socket \n");
		return 0;
	}
	
	//try to connect
	if(connect(sockfd, servinfo->ai_addr, servinfo->ai_addrlen) <0)
	{
		printf("\n Error : Connect Failed \n");
		freeaddrinfo(servinfo);
		return 0;
	}
	
	freeaddrinfo(servinfo);
	return sockfd;
}


-(int)beginListeningOnPort:(int)port
{
	//set up the options
	struct sockaddr_in sin;
	zeroize(&sin, sizeof(sin));
	sin.sin_family = AF_INET;
	sin.sin_port = htons(port);
	sin.sin_addr.s_addr = htonl(INADDR_ANY);
	
    //create the socket
	int s = socket(AF_INET,SOCK_STREAM,0);
	if(s < 0 )
	{
		return NO;
	}
	//set socket options
	int n = 1;
	if (setsockopt (s, SOL_SOCKET, SO_REUSEADDR, (char *)&n, sizeof (n)) < 0)
	{
		NSLog(@"SO_REUSEADDR");
		close (s);
		return NO;
	}
	
	fcntl (s, F_SETFD, 1);
	
    //bind the socket
	if (bind (s, (struct sockaddr *) &sin, sizeof (sin)) < 0)
	{
		fprintf (stderr, "TCP port %d: %s\n", port, strerror (errno));
		close (s);
		
		NSLog(@"Bind Error");
		return NO;
	}
	
    //start listening
	if(listen(s,5) < 0)
	{
		NSLog(@"Listen Error");
		close(s);
		return NO;
		
	}
	
	return s;
}


- (NSData *) readDataFromSocket:(int)socket
{
    
    //make a place to hold the data
	NSMutableData *data = [[NSMutableData alloc]init];
	
	ssize_t bytesReceived = 0;
	int totalBytes = 0;
	
    //create a buffer;
	char buffer[MAXSIZE];
	
	//set the timeout to 10 seconds
	struct timeval tv;
	tv.tv_sec = TIMEOUT;
	tv.tv_usec = 0;
	setsockopt(socket, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv,sizeof(struct timeval));
	
    //keep reading until the socket stops sending
	while (true)
	{
		bytesReceived = read(socket, &buffer, MAXSIZE);
		if(bytesReceived < 0 )
			return nil;
		if(bytesReceived == 0)
			break;
		
		totalBytes+= bytesReceived;
		[data appendBytes:&buffer length:bytesReceived];
		
		
		
	}
	
	return data;
}

- (NSData *) readRequestFromSocket:(int)socket
{
	
	//make a place ot hold the data
	NSMutableData *data = [[NSMutableData alloc]init];
	
	ssize_t bytesReceived = 0;
	int totalBytes = 0;
	
	char buffer[MAXSIZE];
	
	//set the timeout to 10 seconds
	struct timeval tv;
	tv.tv_sec = 120;
	tv.tv_usec = 0;
	setsockopt(socket, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv,sizeof(struct timeval));
	
    //just keep reading until we see the magic /r/n/r/n
	while (true)
	{
		bytesReceived = read(socket, &buffer, MAXSIZE);
		if(bytesReceived < 0 )
			return nil;
		if(bytesReceived == 0)
			break;
		
		totalBytes+= bytesReceived;
		[data appendBytes:&buffer length:bytesReceived];
		
        
        NSString *testString = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    
	
		
        NSRange termRange = [testString rangeOfString:@"\r\n\r\n"];
        
        if(termRange.location != NSNotFound)
		{
			break;
		}
        
		
	}
	
	return data;
}

- (BOOL)writeData:(NSData *)data toSocket:(int)socket
{
	ssize_t currentBytesWritten = 0;
	ssize_t totalBytesWritten = 0;
	int bytesRemaining = (int)data.length;
    
    //set socket options
	int set = 1;
	setsockopt(socket, SOL_SOCKET, SO_NOSIGPIPE, (void *)&set, sizeof(int));
	
	//set the timeout to 10 seconds
	struct timeval tv;
	tv.tv_sec = TIMEOUT;
	tv.tv_usec = 0;
	setsockopt(socket, SOL_SOCKET, SO_SNDTIMEO, (char *)&tv,sizeof(struct timeval));
    
    //keep writing while data is available
	while (bytesRemaining > 0)
	{
		currentBytesWritten = write(socket, &data.bytes[totalBytesWritten],bytesRemaining);
		if(currentBytesWritten < 0)
		{
			return NO;
			
		}
		if(currentBytesWritten == 0)
		{
			if(bytesRemaining == currentBytesWritten)
				return YES;
			else
				return NO;
		}
		
		bytesRemaining -= currentBytesWritten;
		totalBytesWritten += currentBytesWritten;

	}
	
	return YES;
	
}


//this is a workin progress
-(NSData *)stringForError:(int)error
{
	NSString *result;
	
	switch (error) {
		case HTTP_VERSION_NOT_SUPPORTED:
			result = [NSString stringWithFormat:@"HTTP/1.0 505 HTTP Version Not Supported\r\nDate: %@\r\nServer: Adam's Server\r\n\r\n",[NSDate date]];
			break;
        case PAGENOTFOUND:
			result = [NSString stringWithFormat:@"HTTP/1.0 404 Not found\nDate: %@\r\nServer: Adam's Server\r\n\r\n",[NSDate date]];
			break;
        case NOTIMPLEMENTED:
			result = [NSString stringWithFormat:@"HTTP/1.0 501 Not Implemented\r\nDate: %@\r\nServer: Adam's Server\r\n\r\n",[NSDate date]];
			break;
        case BAD_REQUEST:
			result = [NSString stringWithFormat:@"HTTP/1.0 400 Bad Request\r\nDate: %@\r\nServer: Adam's Server\r\n\r\n",[NSDate date]];
			break;
			
		default:
			break;
	}
	
	return [result dataUsingEncoding:NSUTF8StringEncoding];
	
}

- (NSString *)getIPAddress {
	
	NSString *address = @"error";
	struct ifaddrs *interfaces = NULL;
	struct ifaddrs *temp_addr = NULL;
	int success = 0;
	// retrieve the current interfaces - returns 0 on success
	success = getifaddrs(&interfaces);
	if (success == 0) {
		// Loop through linked list of interfaces
		temp_addr = interfaces;
		while(temp_addr != NULL) {
			if(temp_addr->ifa_addr->sa_family == AF_INET) {
				// Check if interface is en0 which is the wifi connection on the iPhone
				if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
					// Get NSString from C String
					address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
					
				}
				
			}
			
			temp_addr = temp_addr->ifa_next;
		}
	}
	// Free memory
	freeifaddrs(interfaces);
	return address;
	
}

-(NSString *)hostFromHTTPURL:(NSString *)httpString
{
    
    
    //ok, manually pull out the host.
    NSRange httpRange = [httpString rangeOfString:@"http://"];
    if(httpRange.location == NSNotFound) return nil;
    
    
    NSString *hostString = [httpString substringFromIndex:(httpRange.location + httpRange.length)];
    
    NSRange slashRange = [hostString rangeOfString:@"/"];
    if(slashRange.location == NSNotFound) return nil;
    
    hostString = [hostString substringToIndex:slashRange.location];
    
    return hostString;
}

-(void)cacheImageHackEnabled:(BOOL)value
{
    self.cache.imageHackEnabled = value;
}

@end
