//
//  PRXProxyServer.h
//  CS4480ProxyServer
//
//  Created by Adam Bradford on 1/12/14.
//  Copyright (c) 2014 Adam Bradford. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PRXProxyServerDelegate <NSObject>

-(void)messageFromProxy:(NSString *)message;
-(void)currentRequestsDidChange:(int)requests;

@end

@interface PRXProxyServer : NSObject

@property (nonatomic, weak) id<PRXProxyServerDelegate> delegate;
@property (nonatomic) int currentConnections;
-(BOOL)startProxyWithPort:(int)port;
-(void)stopProxy;
-(void)emptyCache;
-(void)cacheImageHackEnabled:(BOOL)value;

@end
