//
//  PRXCacheManager.h
//  CS4480ProxyIOS
//
//  Created by Adam Bradford on 1/18/14.
//  Copyright (c) 2014 Adam Bradford. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PRXCacheManager : NSObject

@property (nonatomic) BOOL imageHackEnabled;
-(NSData *)cachedDataForRequest:(NSString *)request;
-(BOOL)cacheData:(NSData *)data forRequest:(NSString *)request;
-(void)clearCache;

@end
