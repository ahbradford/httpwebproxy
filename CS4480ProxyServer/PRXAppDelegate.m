//
//  PRXAppDelegate.m
//  CS4480ProxyServer
//
//  Created by Adam Bradford on 1/12/14.
//  Copyright (c) 2014 Adam Bradford. All rights reserved.
//

#import "PRXAppDelegate.h"
#import "PRXProxyServer.h"

#define kProxyStartString @"Proxy: "

@interface PRXAppDelegate()<PRXProxyServerDelegate>
@property (weak) IBOutlet NSTextField *portTextField;
@property (weak) IBOutlet NSButton *startButton;
@property (unsafe_unretained) IBOutlet NSTextView *textView;

@property (strong,nonatomic) PRXProxyServer *proxy;
@property (nonatomic) BOOL proxyRunning;
@property (weak) IBOutlet NSTextField *connectionsTextField;

@property (strong,nonatomic) NSTimer *connectionsTimer;

@end
@implementation PRXAppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application
    self.proxy = [[PRXProxyServer alloc]init];
    self.proxy.delegate = self;
    
    
}
- (IBAction)startButtonPressed:(id)sender
{
    if(!self.proxyRunning)
    {
        self.proxyRunning = YES;
        [self.textView setString:@""];
        [self.proxy emptyCache];
        int port = self.portTextField.intValue;
        BOOL success = [self.proxy startProxyWithPort:port];
        
        self.connectionsTimer = [NSTimer scheduledTimerWithTimeInterval:.1f target:self selector:@selector(updateCurrentRequests) userInfo:nil repeats:YES];
        if(success)
        {
            
            [self.startButton setTitle:@"Stop Proxy"];
        }
    }
    else
    {
        [self.proxy stopProxy];
        [self.connectionsTimer invalidate];
        self.connectionsTimer = nil;
        
        [self.startButton setTitle:@"Start Proxy"];
        self.proxyRunning = NO;
    }
}

-(void)messageFromProxy:(NSString *)message
{
    if(message)
    {
    [[NSOperationQueue mainQueue]addOperationWithBlock:^{
        NSString *newMessage = [kProxyStartString stringByAppendingString:message];
        [self.textView setString:[self.textView.string stringByAppendingFormat:@"%@\n",newMessage]];
    }];
       
    }
}
- (IBAction)cacheImageHackPressed:(NSButton *)sender
{
    if([sender.title isEqualToString:@"Cache Image Hack"])
    {
        sender.title = @"Stop Cache Image Hack";
        [self.proxy cacheImageHackEnabled:YES];
    }
    else
    {
        sender.title = @"Cache Image Hack";
       [self.proxy cacheImageHackEnabled:NO];
    }
}
- (IBAction)emptyCachePressed:(id)sender
{
    [self.proxy emptyCache];
}
-(void)messageSetToRemote:(NSString *)message
{
    
}

-(void)updateCurrentRequests
{
    self.connectionsTextField.integerValue = self.proxy.currentConnections;
}

-(void)currentRequestsDidChange:(int)requests
{
    self.connectionsTextField.integerValue = requests;
}
-(void)messageReceivedFromRemote:(NSString *)message
{
    
}
@end
