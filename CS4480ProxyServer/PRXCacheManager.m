//
//  PRXCacheManager.m
//  CS4480ProxyIOS
//
//  Created by Adam Bradford on 1/18/14.
//  Copyright (c) 2014 Adam Bradford. All rights reserved.
//

#import "PRXCacheManager.h"

@interface PRXCacheManager()
@property (nonatomic, strong) NSString *cacheDir;
@property (nonatomic) dispatch_queue_t fileQueue;

@end

@implementation PRXCacheManager


-(instancetype)init
{
	self = [super init];
	if(self)
	{
        //initalize the ivars
		_cacheDir = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
        _cacheDir = [_cacheDir stringByAppendingPathComponent:@"com.adambradford.cs4480proxy.cache"];
        [[NSFileManager defaultManager] removeItemAtPath:_cacheDir error:nil];
        [[NSFileManager defaultManager]createDirectoryAtPath:_cacheDir withIntermediateDirectories:NO attributes:nil error:nil];
		_fileQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        
        self.imageHackEnabled = NO;
		
		NSLog(@"%@",_cacheDir);
	}
	return self;
    
}
-(NSData *)cachedDataForRequest:(NSString *)request
{
    //set up our file to look for
	request = [request stringByReplacingOccurrencesOfString:@"/" withString:@"."];
	request = [request stringByReplacingOccurrencesOfString:@":" withString:@"."];
	
	NSString *filePath = [self.cacheDir stringByAppendingPathComponent:request];
    
    
    if(self.imageHackEnabled)
    {
        
        
        if([filePath rangeOfString:@"jpg"].location != NSNotFound||
           [filePath rangeOfString:@"png"].location != NSNotFound ||
           [filePath rangeOfString:@"gif"].location != NSNotFound ||
           [filePath rangeOfString:@"jpeg"].location != NSNotFound)
        {
            NSURL *fakeImageURL = [[NSBundle mainBundle] URLForResource:@"img" withExtension:@"dat"];
            NSData *data =  [NSData dataWithContentsOfURL:fakeImageURL];
            return data;
        }
        
    }
    
	
	__block NSData *resultData = nil;
	dispatch_sync(_fileQueue, ^{
        
        //check if the file exists
		BOOL exists = [[NSFileManager defaultManager]fileExistsAtPath:filePath isDirectory:NO];
		if(exists	)
		{
			resultData = [NSData dataWithContentsOfFile:filePath];
			
		}
	});
	
    //return either the data or nil
	return resultData;
	
	
}

-(BOOL)cacheData:(NSData *)data forRequest:(NSString *)request
{
    //set up the filename to cache under
	request = [request stringByReplacingOccurrencesOfString:@"/" withString:@"."];
	request = [request stringByReplacingOccurrencesOfString:@":" withString:@"."];
	
	NSString *filePath = [self.cacheDir stringByAppendingPathComponent:request];
	__block BOOL success = YES;
	
	dispatch_sync(_fileQueue, ^{
		
        //Does it exist?
		BOOL exists = [[NSFileManager defaultManager]fileExistsAtPath:filePath isDirectory:NO];
		
		if(exists)
		{
            //The file should be updated, so delete it
			NSError *error;
			[[NSFileManager defaultManager]removeItemAtPath:filePath error:&error];
			if(error)
			{
				NSLog(@"%@",error.localizedDescription);
				success =  NO;
				
			}
		}
		else
		{
            //did we sucessfully write the data to disk?
			success = [data writeToFile:filePath atomically:YES];
		}
	});
	
	return success;
	
}

-(void)clearCache
{
	dispatch_sync(_fileQueue, ^{
		NSArray *files = [[NSFileManager defaultManager]contentsOfDirectoryAtPath:self.cacheDir error:nil];
        
        //just delete all the files in the cache directory
		for(NSString *file in files)
		{
			NSString *filePath = [self.cacheDir stringByAppendingPathComponent:file];
			[[NSFileManager defaultManager]removeItemAtPath:filePath error:nil];
		}
	});
	
}

@end

